import pygame
from random import choice

RES = WIDTH, HEIGHT = 1700, 1300
TITLE = 50
cols, rows = WIDTH // TITLE, HEIGHT // TITLE

pygame.init()
sc = pygame.display.set_mode(RES)
clock = pygame.time.Clock()


class Cell:

    def __init__(self, x, y):
        self.x, self.y = x, y
        self.walls = {'top': True, 'right': True, 'bottom': True, 'left': True}
        self.visited = False

    def draw_current_cell(self):
        x, y = self.x * TITLE, self.y * TITLE
        pygame.draw.rect(sc, pygame.Color('saddlebrown'), (x + 2, y + 2, TITLE - 2, TITLE - 2))

    def draw(self):
        x, y = self.x * TITLE, self.y * TITLE
        if self.visited:
            pygame.draw.rect(sc, pygame.Color('black'), (x, y, TITLE, TITLE))

        if self.walls['top']:
            pygame.draw.line(sc, pygame.Color('darkorange'), (x, y), (x + TITLE, y), 2)
        if self.walls['right']:
            pygame.draw.line(sc, pygame.Color('darkorange'), (x + TITLE, y), (x + TITLE, y + TITLE), 2)
        if self.walls['bottom']:
            pygame.draw.line(sc, pygame.Color('darkorange'), (x + TITLE, y + TITLE), (x, y + TITLE), 2)
        if self.walls['left']:
            pygame.draw.line(sc, pygame.Color('darkorange'), (x, y + TITLE), (x, y), 2)

    def cell_check(self, x, y):
        find_index = lambda x, y: x + y * cols
        if x < 0 or x > cols - 1 or y < 0 or y > rows - 1:
            return False
        return grid_cells[find_index(x, y)]

    def check_neighbors(self):
        neighbors = []
        top = self.cell_check(self.x, self.y - 1)
        right = self.cell_check(self.x + 1, self.y)
        bottom = self.cell_check(self.x, self.y + 1)
        left = self.cell_check(self.x - 1, self.y)
        if top and not top.visited:
            neighbors.append(top)
        if right and not right.visited:
            neighbors.append(right)
        if bottom and not bottom.visited:
            neighbors.append(bottom)
        if left and not left.visited:
            neighbors.append(left)
        return choice(neighbors) if neighbors else False


def remove_walls(current, nexts):
    dx = current.x - nexts.x
    if dx == 1:
        current.walls['left'] = False
        nexts.walls['right'] = False
    elif dx == -1:
        current.walls['right'] = False
        nexts.walls['left'] = False
    dy = current.y - nexts.y
    if dy == 1:
        current.walls['top'] = False
        nexts.walls['bottom'] = False
    elif dy == -1:
        current.walls['bottom'] = False
        nexts.walls['top'] = False


grid_cells = [Cell(col, row) for row in range(rows) for col in range(cols)]
current_cell = grid_cells[0]
stack = []
colors, color = [], 10

while True:
    sc.fill(pygame.Color('darkslategray'))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()

    [cell.draw() for cell in grid_cells]
    current_cell.visited = True
    current_cell.draw_current_cell()
    [pygame.draw.rect(sc, colors[i], (cell.x * TITLE + 5, cell.y * TITLE + 5, TITLE - 10, TITLE - 10),
                      border_radius=12) for i, cell in enumerate(stack)]

    next_cell = current_cell.check_neighbors()
    if next_cell:
        next_cell.visited = True
        stack.append(current_cell)
        colors.append((min(color, 255), 100, 1))
        color += 0.5
        remove_walls(current_cell, next_cell)
        current_cell = next_cell
    elif stack:
        current_cell = stack.pop()

    pygame.display.flip()
    clock.tick(120)
